(** Electron spin.

Note :
[Alfa] if written with an 'f' instead of 'ph' because it has the same number of
letters as [Beta], so the alignment of the code is nicer.

*)

type t = Alfa | Beta

val other : t -> t


