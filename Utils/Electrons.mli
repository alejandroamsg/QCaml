(** Information related to electrons. *)

type t 

val make : ?multiplicity:int -> ?charge:int -> Nuclei.t -> t
(** Creates the data relative to electrons for a molecular system
  described by {!Nuclei.t} for a given total charge and spin multiplicity.
  @param multiplicity default is 1
  @param charge default is 0
  @raise Invalid_argument if the spin multiplicity is not compatible with
    the molecule and the total charge.
  *)

val charge : t -> Charge.t
(** Sum of the charges of the electrons. *)

val n_elec : t -> int
(** Number of electrons *)

val n_alfa : t -> int
(** Number of alpha electrons *)

val n_beta : t -> int
(** Number of beta  electrons *)

val multiplicity : t -> int 
(** Spin multiplicity: {% $2S+1$ %} *)

