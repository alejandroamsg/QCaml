(** Hash table where the keys are of type Zkey.t (tuples of integers) *)

module Zmap = Hashtbl.Make(Zkey)
include Zmap

