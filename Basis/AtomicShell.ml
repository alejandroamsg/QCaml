open Util
open Constants

type t = {
  expo      : float array array;
  coef      : float array array;
  norm_coef : float array array;
  norm_coef_scale : float array;
  contr     : ContractedShell.t array;
  index     : int;
  center    : Coordinate.t;
  ang_mom   : AngularMomentum.t;
}

module Am = AngularMomentum
module Co = Coordinate
module Cs = ContractedShell


let make ?(index=0) contr = 
  assert (Array.length contr > 0);

  let coef = Array.map Cs.coefficients contr
  and expo = Array.map Cs.exponents contr
  in

  let center = Cs.center contr.(0) in
  let rec unique_center = function
  | 0 -> true
  | i -> if Cs.center contr.(i) = center then (unique_center [@tailcall]) (i-1) else false
  in
  if not (unique_center (Array.length contr - 1)) then
    invalid_arg "ContractedAtomicShell.make Coordinate.t differ";
    
  let ang_mom = Cs.ang_mom contr.(0) in
  let rec unique_angmom = function
  | 0 -> true
  | i -> if Cs.ang_mom contr.(i) = ang_mom then (unique_angmom [@tailcall]) (i-1) else false
  in
  if not (unique_angmom (Array.length contr - 1)) then
    invalid_arg "ContractedShell.make: AngularMomentum.t differ";
    
  let norm_coef =
    Array.map Cs.normalizations contr 
  in
  let norm_coef_scale = Cs.norm_scales contr.(0)
  in
  { index ; expo ; coef ; center ; ang_mom ; norm_coef ;
    norm_coef_scale ; contr  }


let with_index a i =
  { a with index = i }


let exponents x = x.expo

let coefficients x = x.coef

let center x = x.center

let ang_mom x = x.ang_mom

let size x = Array.length x.contr

let normalizations x = x.norm_coef

let norm_scales x = x.norm_coef_scale

let index x = x.index

let size_of_shell x = Array.length x.norm_coef_scale

let contracted_shells x = x.contr


(** {2 Printers} *)

open Format

let pp_debug ppf x =
  fprintf ppf "@[<2>{@ ";
  fprintf ppf "@[<2>expo =@ %a ;@]@ " pp_float_2darray_size x.expo;
  fprintf ppf "@[<2>coef =@ %a ;@]@ " pp_float_2darray_size x.coef;
  fprintf ppf "@[<2>center =@ %a ;@]@ " Co.pp_angstrom x.center;
  fprintf ppf "@[<2>ang_mom =@ %a ;@]@ " Am.pp_string x.ang_mom;
  fprintf ppf "@[<2>norm_coef =@ %a ;@]@ " pp_float_2darray_size x.norm_coef;
  fprintf ppf "@[<2>norm_coef_scale =@ %a ;@]@ " pp_float_array_size x.norm_coef_scale;
  fprintf ppf "@[<2>index =@ %d ;@]@ " x.index;
  fprintf ppf "}@,@]"

let pp ppf s =
  fprintf ppf "@[%3d-%-3d@]" (s.index+1) (s.index+ (size_of_shell s)*(size s));
  fprintf ppf "@[%a@ %a@] @[" Am.pp_string s.ang_mom Co.pp s.center;
  Array.iter2 (fun e_arr c_arr -> fprintf ppf "@[<v>";
    Array.iter2 (fun e c -> fprintf ppf "@[%16.8e  %16.8e@]@;" e c)
    e_arr c_arr;
    fprintf ppf "@;@]") s.expo s.coef;
  fprintf ppf "@]"



