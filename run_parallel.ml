
let pouet () =
  Printf.printf "Hello from rank %d of %d.\n" Parallel.rank Parallel.size;
  let () = Parallel.barrier () in
(*
let v = Parallel.Vec.init 47 (fun i -> float_of_int i) in
*)
  let a = Array.init 41 (fun i -> float_of_int (i+1)) |> Lacaml.D.Vec.of_array in
  let b = Array.init 41 (fun i -> float_of_int (3*i+1)) |> Lacaml.D.Vec.of_array in
  let v1 = Parallel.Vec.of_vec a in
  let v2 = Parallel.Vec.of_vec b in
  let d1 = Parallel.dot v1 v2 |> Parallel.broadcast_float in
  let d2 = Lacaml.D.dot a b in
  (*
  let w = Parallel.Vec.to_vec v in
  Format.printf "%a" Parallel.Vec.pp v;
  if Parallel.master then
      Format.printf "@[%a@]@;" (Lacaml.Io.pp_lfvec ()) w;
  *)
  Printf.printf "%f %f\n" d1 d2;
  print_newline ()


let pouet2 () =
  let f (a,b) = (Parallel.rank, a+b) in
  let input  = Stream.of_list
    [ (1,2) ; (3,4) ; (5,6) ; (7,8) ; (9,10)  
    ; (1,2) ; (3,4) ; (5,6) ; (7,8) ; (9,10)  
    ; (1,2) ; (3,4) ; (5,6) ; (7,8) ; (9,10)  
    ; (1,2) ; (3,4) ; (5,6) ; (7,8) ; (9,10) ]
  in
  let stream =
    Farm.run ~f input
  in
  Stream.iter (fun (x,y) -> Printf.printf "%d  %d\n%!" x y) stream


open Printf
let () = 
  printf "%d : %s\n" Parallel.rank Parallel.Node.name;
  let value = Parallel.broadcast (lazy Parallel.rank) in
  printf "%d : %d\n" Parallel.rank value;
  let value = Parallel.Node.broadcast (lazy Parallel.rank) in
  printf "%d : %d\n" Parallel.rank value
