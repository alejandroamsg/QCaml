(** A datastructure to represent pairs of atomic shells.  
*)

type t


val make : ?cutoff:float -> AtomicShell.t -> AtomicShell.t -> t option
(** Creates an atomic shell pair from two atomic shells.

    The contracted shell pairs contains the only pairs of primitives for which
    the norm is greater than [cutoff]. 

    If all the contracted shell pairs are not significant, the function returns
    [None].
*)

val of_atomic_shell_array : ?cutoff:float -> AtomicShell.t array -> t option array array
(** Creates all possible atomic shell pairs from an array of atomic shells.
    If an atomic shell pair is not significant, sets the value to [None].
*)

val atomic_shell_a : t -> AtomicShell.t
(** Returns the first {!AtomicShell.t} which was used to build the atomic
    shell pair.
*)

val atomic_shell_b : t -> AtomicShell.t
(** Returns the second {!AtomicShell.t} which was used to build the atomic
    shell pair.
*)

val contracted_shell_pairs : t -> ContractedShellPair.t list
(** Returns an array of {!ContractedShellPair.t}, containing all the pairs of
    contracted functions used to build the atomic shell pair.
*)

val a_minus_b : t -> Coordinate.t
  (* A-B *)

val a_minus_b_sq : t -> float
  (* |A-B|^2 *)

val norm_scales : t -> float array
  (* norm_coef.(i) / norm_coef.(0) *)

val ang_mom : t -> AngularMomentum.t
  (* Total angular Momentum *)

val monocentric : t -> bool
(** If true, the two atomic shells have the same center. *)

