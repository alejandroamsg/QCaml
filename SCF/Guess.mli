open Lacaml.D

(** Guess for Hartree-Fock calculations. *)

type guess = 
| Hcore  of Mat.t  (* Core Hamiltonian Matrix *)
| Huckel of Mat.t  (* Huckel Hamiltonian Matrix *)
| Matrix of Mat.t  (* Guess Eigenvectors *)

type t = guess


val make : ?nocc:int -> guess:[ `Hcore | `Huckel | `Matrix of Mat.t ] -> AOBasis.t -> t


(** {2 Tests} *)

val test_case : AOBasis.t -> unit Alcotest.test_case list
