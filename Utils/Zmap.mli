(** Hash table where the keys are of type Zkey.t (tuples of integers). *)

include module type of Hashtbl.Make(Zkey)


