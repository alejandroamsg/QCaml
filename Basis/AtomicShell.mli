(** Set of contracted Gaussians differing only by the powers of x, y and z, with a
    constant {!AngularMomentum.t}, all centered on the same center.

    In other words, it is the set of all contracted shells sharing the same center.

{%
\begin{align*}
\chi_{n_x,n_y,n_z}(r) & = f(n_x,n_y,n_z) \sum_{j=1}^{n} \sum_{i=1}^{m} \mathcal{N}_{ij}\, d_{ij}\, g_{ij\,n_x,n_y,n_z}(r) \\
        & = (x-X_A)^{n_x} (y-Y_A)^{n_y} (z-Z_A)^{n_z} f(n_x,n_y,n_z) \sum_{j=1}^{n} \sum_{i=1}^{m} \mathcal{N}_{ij}\, d_{ij}\, \exp \left( -\alpha_{ij} |r-R_A|^2 \right)
\end{align*}
%}

where:

- {% $g_{ij\,n_x,n_y,n_z}(r)$ %} is the i-th {!PrimitiveShell.t} of the j-th {!ContractedShell.t} 

- {% $n_x + n_y + n_z = l$ %}, the total angular momentum

- {% $\alpha_{ij}$ %} are the exponents (tabulated) of the j-th {!ContractedShell.t} 

- {% $d_{ij}$ %} are the contraction coefficients of the j-th {!ContractedShell.t} 

- {% $\mathcal{N}_{ij}$ %} is the normalization coefficient of the i-th primitive shell
  ({!PrimitiveShell.norm_coef}) of the j-th {!ContractedShell.t} 

- {% $f(n_x,n_y,n_z)$ %} is a scaling factor adjusting the normalization coefficient for the
  particular powers of {% $x,y,z$ %} ({!PrimitiveShell.norm_coef_scale})

*)

type t 

val make : ?index:int -> ContractedShell.t array -> t 
(** Creates a contracted shell from a list of coefficients and primitives.  *)

val with_index  : t -> int -> t
(** Returns a copy of the contracted shell with a modified index. *)

val index : t -> int
(** Index in the basis set, represented as an array of contracted shells. *)

val center : t -> Coordinate.t
(** Coordinate of the center {% $\mathbf{A} = (X_A,Y_A,Z_A)$ %}. *)

val ang_mom : t -> AngularMomentum.t
(** Total angular momentum : {% $l = n_x + n_y + n_z$ %}. *)

val size : t -> int
(** Number of contracted functions, {% $n$ %} in the definition. *)

val contracted_shells: t -> ContractedShell.t array
(** Array of contracted gaussians *)

val exponents : t -> float array array
(** Array of exponents {% $\alpha_{ij}$ %}. The first index is the index of
    the contracted function, and the second index is the index of the primitive.
*)

val coefficients : t -> float array array
(** Array of contraction coefficients {% $d_{ij}$ %}. The first index is the index of
    the contracted function, and the second index is the index of the primitive.
*)

val normalizations : t -> float array array
(** Normalization coefficients {% $\mathcal{N}_{ij}$ %}. The first index is the index of
    the contracted function, and the second index is the index of the primitive.
*)

val norm_scales : t -> float array
(** Scaling factors {% $f(n_x,n_y,n_z)$ %}, given in the same order as
    [AngularMomentum.zkey_array ang_mom]. *)

val size_of_shell : t -> int
(** Number of contracted functions in the shell: length of {!norm_coef_scale}. *)


(** {2 Printers} *)

val pp : Format.formatter -> t -> unit

