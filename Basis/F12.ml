(** Two electron integral functor for operators that are separable among %{ $(x,y,z)$ %}.
    It is parameterized by the [zero_m] function.
*)

open Constants
let cutoff = integrals_cutoff

module T = struct

  let name = "F12"

  let class_of_contracted_shell_pair_couple ~basis shell_pair_couple = 
    let f12 =
      match Basis.f12 basis with
      | Some f12 -> f12
      | None -> invalid_arg "f12 factor should not be None"
    in 
    let g = f12.F12factor.gaussian in
    F12RR.contracted_class_shell_pair_couple ~basis 
      g.GaussianOperator.expo_g_inv
      g.GaussianOperator.coef_g
      shell_pair_couple

end

module M = TwoElectronIntegrals.Make(T)
include M



