(** A point in a 3D space. Used by {!Angstrom.t} and {!Bohr.t}.
 *)

type t = {
  x : float ;
  y : float ;
  z : float ;
}


