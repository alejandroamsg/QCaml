let create ?(temp_dir="/dev/shm")  data_type  size_array =
    let result = 
      Bigarray.Genarray.create data_type Bigarray.fortran_layout size_array
    in
    Bigarray.Genarray.fill result 0.;
    result

