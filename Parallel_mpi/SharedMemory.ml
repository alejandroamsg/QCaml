let create ?(temp_dir="/dev/shm")  data_type  size_array =
    let filename =
      Parallel.Node.broadcast (lazy (Filename.temp_file ~temp_dir "4idx." ".tmp"))
    in

    if Parallel.Node.master then
      begin
        let fd = Unix.openfile filename Unix.[O_RDWR ; O_CREAT] 0o777 in
        let result =
          Unix.map_file fd  data_type  Bigarray.fortran_layout  true  size_array
        in
        Bigarray.Genarray.fill result 0.;
        Parallel.Node.barrier ();
        at_exit (fun () -> Unix.close fd ; try Sys.remove filename with _ -> ());
        result
      end
    else
      begin
        Parallel.Node.barrier ();
        let fd = Unix.openfile filename [Unix.O_RDONLY] 0o777 in
        at_exit (fun () -> Unix.close fd ; try Sys.remove filename with _ -> ());
        Unix.map_file fd  data_type  Bigarray.fortran_layout  false  size_array
      end

