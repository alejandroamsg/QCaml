(** The Farm skeleton, similar to SklMl.

The input is a stream of input data, and the output is a stream of data.
*)


val run_sequential : ('a -> 'b) -> 'a Stream.t -> 'b Stream.t

val run : ?ordered:bool -> ?comm:Mpi.communicator ->
  f:('a -> 'b) -> 'a Stream.t -> 'b Stream.t
(** Run the [f] function on every process by popping elements from the
    input stream, and putting the results on the output stream. If [ordered]
    (the default is [ordered = true], then the order of the output is kept
    consistent with the order of the input.
    [comm], within MPI is a communicator. It describes a subgroup of processes.
*)


