#include <math.h>
#include <caml/mlvalues.h>
#include <caml/alloc.h>


CAMLprim value erf_float_bytecode(value x)
{
  return copy_double(erf(Double_val(x)));
}  

CAMLprim double erf_float(double x)
{
  return erf(x);
}


CAMLprim value erfc_float_bytecode(value x)
{
  return copy_double(erfc(Double_val(x)));
}  

CAMLprim double erfc_float(double x)
{
  return erfc(x);
}


CAMLprim value gamma_float_bytecode(value x)
{
  return copy_double(tgamma(Double_val(x)));
}  


CAMLprim double gamma_float(double x)
{
  return tgamma(x);
}


#include <stdio.h>
CAMLprim int32_t popcnt(int64_t i)
{
  return __builtin_popcountll (i);
}


CAMLprim value popcnt_bytecode(value i)
{
  return caml_copy_int32(__builtin_popcountll (Int64_val(i)));
}


CAMLprim int32_t trailz(int64_t i)
{
  return __builtin_ctzll (i);
}


CAMLprim value trailz_bytecode(value i)
{
  return caml_copy_int32(__builtin_ctzll (Int64_val(i)));
}

CAMLprim int32_t leadz(int64_t i)
{
  return __builtin_clzll(i);
}


CAMLprim value leadz_bytecode(value i)
{
  return caml_copy_int32(__builtin_clzll (Int64_val(i)));
}



#include <unistd.h>

CAMLprim value unix_vfork(value unit)
{
  int ret;
  ret = vfork();
  return Val_int(ret);
}

