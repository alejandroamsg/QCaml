(** Type for the Fock operator in AO basis. *)

open Lacaml.D


type t 

(** {1 Accessors} *)

val fock : t -> Mat.t
(** Fock matrix in AO basis *)

val core : t -> Mat.t
(** Core Hamiltonian : {% $\langle i | \hat{h} | j \rangle$ %} *)

val coulomb : t -> Mat.t
(** Coulomb matrix : {% $\langle i | J | j \rangle$ %} *)

val exchange : t -> Mat.t
(** Exchange matrix : {% $\langle i | K | j \rangle$ %} *)


(** {1 Creators} *)

val make_rhf : density:Mat.t -> ?threshold:float -> AOBasis.t -> t
(** Create a Fock operator in the RHF formalism. Expected density is
    {% $2 \mathbf{C\, C}^\dagger$ %}. [threshold] is a threshold on the
    integrals. *)

val make_uhf : density_same: Mat.t -> density_other:Mat.t -> ?threshold:float ->
               AOBasis.t -> t
(** Create a Fock operator in the UHF formalism. Expected density is
    {% $\mathbf{C\, C}^\dagger$ %}. When building the {% $\alpha$ %} Fock
    operator, [density_same] is the {% $\alpha$ %} density and [density_other]
    is the {% $\beta$ %} density. [threshold] is a threshold on the integrals. *)


(** {1 Operations} *)

val add : t -> t -> t
(** Add two Fock operators sharing the same core Hamiltonian. *)

val sub : t -> t -> t
(** Subtract two Fock operators sharing the same core Hamiltonian. *)


(** {1 Printers} *)

val pp : Format.formatter -> t -> unit

