type t = float

external of_float : float -> t = "%identity"
external to_float : t -> float = "%identity"

let of_int i = float_of_int i
let of_string s = float_of_string s

let to_int   x = int_of_float x
let to_string x = 
  if x >= 0. then
    Printf.sprintf "+%f" x
  else
    Printf.sprintf "%f" x

let ( + ) a b =
  (to_float a) +. (to_float b) |> of_float

let ( - ) a b =
  (to_float a) -. (to_float b) |> of_float

let ( * ) a b =
  (to_float a) *. b |> of_float

let ( / ) a b =
  (to_float a) /. b |> of_float

let pp ppf x = 
  if x > 0. then
    Format.fprintf ppf "@[+%f@]" (to_float x)
  else
    Format.fprintf ppf "@[%f@]" (to_float x)
