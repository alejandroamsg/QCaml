(** Floats >= 0. *)
type t = private float
val of_float : float -> t
val unsafe_of_float : float -> t
val to_float : t -> float
val to_string : t -> string
val of_string : string -> t
