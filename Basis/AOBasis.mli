(** Data structure for Atomic Orbitals. *)

open Lacaml.D

type t

(** {1 Accessors} *)

val basis : t -> Basis.t
(** One-electron basis set *)

val overlap : t -> Overlap.t
(** Overlap matrix *)

val multipole : t -> Multipole.t
(** Multipole matrices *)

val ortho : t -> Orthonormalization.t
(** Orthonormalization matrix of the overlap *)

val eN_ints : t -> NucInt.t
(** Electron-nucleus potential integrals *)

val ee_ints : t -> ERI.t
(** Electron-electron potential integrals *)

val ee_lr_ints : t -> ERI_lr.t
(** Electron-electron long-range potential integrals *)

val f12_ints : t -> F12.t
(** Electron-electron potential integrals *)

val kin_ints : t -> KinInt.t
(** Kinetic energy integrals *)

val cartesian : t -> bool
(** If true, use cartesian Gaussians (6d, 10f, ...) *)

val values : t -> Coordinate.t -> Vec.t
(** Values of the AOs evaluated at a given point *)



(** {1 Creators} *)

val make : cartesian:bool -> basis:Basis.t ->  ?f12:F12factor.t ->  Nuclei.t -> t
(** Creates the data structure for atomic orbitals from a {Basis.t} and the
    molecular geometry {Nuclei.t} *)


(** {2 Tests} *)

val test_case : string -> t -> unit Alcotest.test_case list
