# Generic installation

```bash
opam install ocamlbuild ocamlfind lacaml mpi getopt alcotest zarith
```


# BLAS/Lapack

Install OpenBLAS from your system package manager, for example:

```bash
sudo apt-get install libopenblas-dev
```

# LaCAML

LaCAML is the OCaml binding to the LAPACK library.

```bash
opam install lacaml
```

To use MKL with LaCaml:

```bash
export  LACAML_LIBS="-L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_rt -lpthread -lm -ldl"
opam install lacaml
```

# MPI

MPI is the Message Passing Interface, required for distributed parallelism.
For large MPI calculations, a version >= 4.09.0 of the compiler is required.

```bash
opam install mpi
```

To use Intel MPI

```bash
export MPI_INC_DIR=${I_MPI_ROOT}/include64/
export MPI_LIB_DIR=${I_MPI_ROOT}/lib64/
export MPI_BIN_PATH=${I_MPI_ROOT}/bin64/
opam install mpi
```

# odoc-ltxhtml

This plugin allows to embed equations in the documentation generated by Ocamldoc.

Download the source code [here](https://github.com/scemama/odoc-ltxhtml). 

```bash
git clone https://github.com/scemama/odoc-ltxhtml
cd odoc-ltxhtml
make install
```

# Getopt

Parsing of command line arguments (similar to GNU GetOpt)

```bash
opam install getopt
```

# Alcotest

Lightweight and colourful test framework

```bash
opam install alcotest
```

# Zarith

Implements arithmetic and logical operations over arbitrary-precision integer

```bash
opam install zarith
```

