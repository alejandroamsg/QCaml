(** Data structure for the molecular geometry, represented as an array
of tuples ({!Element.t}, {!Coordinate.t}).
*)

type t = (Element.t * Coordinate.t) array


val of_xyz_string : string -> t
(** Create from a string, in [xyz] format. *)

val of_xyz_file : string -> t
(** Create from a file, in [xyz] format. *)

val of_zmt_string : string -> t
(** Create from a string, in z-matrix format. *)

val of_zmt_file : string -> t
(** Create from a file, in z-matrix format. *)

val to_string : t -> string
(** Transform to a string, for printing. *)

val of_filename : string -> t
(** Detects the type of file ([xyz], z-matrix) and reads the file. *)


val repulsion : t -> float
(** Nuclear repulsion energy, in atomic units. *)

val charge : t -> Charge.t
(** Sum of the charges of the nuclei. *)

val small_core : t -> int
(** Number of core electrons in the small core model. *)

val to_xyz_string  : t -> string
val to_t2_string  : t -> string
