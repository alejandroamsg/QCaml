(* Single process function *)
let run_sequential f stream =

  let next _ =
    try
      let task = Stream.next stream in
      Some (f task) 
    with Stream.Failure -> None in
    Stream.from next

let run ?(ordered=true) ?(comm) ~f stream =
  run_sequential f stream

